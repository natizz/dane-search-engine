# Dane Search Engine

One Paragraph of project description goes here

## Getting Started

```
npm install
node start app.js
```

### Prerequisites

```
package.json
```

### Installing



## Running the tests

```
npm test
```

### And coding style tests


```
es6 
```

## Deployment

* Based on bitbucket pipline, deploying to heroku app engine


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Search for engine - Example
```
curl -XPOST -H "Content-type: application/json" -d '{
   "features": {
            "ram_range":{
                max:'128',
                min:'1'
            },
            "cpu":[],
            "screenSize":[7,8,9,10,11,12],
            "priceRange":{
                "max":99999,
                min:400
            },
            "warranty":'true',
            "keyworkds":'3g'
        }
}' 'https://2c8cx5whk0.execute-api.us-east-1.amazonaws.com/v1/laptop'
```

```
Payload

[
  {
    "cardTitle": "Laptop title",
    "cardSubtitle": "Laptop Price",
    "cardImage": "image url",
    "cardLink": "Afiileate url",
    "buttons": [
		{//Link to buy item
			"buttonText":"BUY",
			"buttonType":"url",
			"target":"Affiliate url"
		},
		{//Link fo rrecomendations and rating
			"buttonText":"Reviews",
			"buttonType":"url",
			"target":"Reviews link"
		}
	]
  }
]
```