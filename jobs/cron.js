/**
 * Created by danielbachar on 20/08/2017.
 */
const CronJob = require('cron').CronJob;
const request = require('requestretry');

const productScraper = require('../lib/scrapers/productScraper');
const reviewsScraper = require('../lib/scrapers/reviewsScraper');


const reviewScraperJob = new CronJob('* * * * * */7',() => {
  console.log(`Reviews update job - ${new Date()}`);
  reviewsScraper.run();
  }, () => {
  console.log("Reviews Scrapper Stopped.");
}, false);

const updateDBJob = new CronJob('* * * * * */7',() => {//evey sunday
    productScraper.run();
    productUpdater.run();

  },function () {
    /* This function is executed when the job stops */
    console.log("Filling DB Job is Stop");
  },
  true /* Start the job right now */
);

function runUpdateDBJobOnce() {
  productScraper.run();
}

const serverTest = new CronJob('*/12 * * * *',() => {//every 12 min
    console.log('Start server ping job - ',new Date());
    const search_params = {
      ram_range: {
        max: '128',
        min: '8'
      },
      cpu: [],
      screenSize: [15,16,17],
      priceRange: {
        max: 99999,
        min: 800
      },
      warranty: 'true',
      shouldNOTSearch:'true'
    };

    const url = 'https://dane-search-engine.herokuapp.com/api/search/category/laptop';
    const url_test = "https://3bd97bd4.ngrok.io/api/search/category/laptop";//"https://41ee807e.ngrok.io/api/search/category/laptop"
    const options = {
      url: url,
      method: "POST",
      json: true,   // <--Very important!!!
      body: {features: search_params}
    };

    request(options,(err,res,body) => {
      console.log('Got request back - ',new Date());
      console.log('Request body - ',body);
    });

  },function () {
    /* This function is executed when the job stops */
    console.log("Server Ping Job Has Stopped");
  },
  true /* Start the job right now */
);

function initAllJobs() {
  console.log('init server cron jobs');
  serverTest.start();
  console.log('init DB filling job');
  updateDBJob.start();

  reviewScraperJob.start();

}

module.exports = {
  initAllJobs,
  runUpdateDBJobOnce
};