'use strict';

const express = require('express');
const router = express.Router();

const engine = require('./../lib/engine/engine');

router.post('/category/:category', (req, res) => {
    const category = req.params.category;
    const params = req.body || {};

    engine.searchCategory({category, params}, (err, results) => {
    if (err) {
      res.status(err.status || 500).send(err.message);
    } else {
      res.json(results);
    }
    });
});

module.exports = router;