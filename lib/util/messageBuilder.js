/**
 * Created by danielbachar on 03/09/2017.
 */


function buildMessageFor(items) {
    var cards = [];

    items.forEach((item) => {
        cards.push({
            cardTitle:item.title ? (item.title.length > 79 ? item.title.slice(0,79) : item.title) : '', // max 80 chars
            cardSubtitle:item.price ? ('Price - ' + item.price.length > 70 ? item.price.slice(0,70) : item.price) : 'Missing Price', // max 80 chars,//
            cardImage:item.image,
            cardLink:item.url,
            buttons:getCardButtons(item)
        });
    });
    console.log('finished  building Cards');
    return cards;
}


function getReviewTitleFor(item) {

    let title = "";

    if (item.reviews && item.reviews[0] && item.reviews[0].rating >= 4) {
        title = "Experts Favorite"
    } else if (item.normalizedRating > 0.8) {
        title = "Crowd Favorite";
    }
    title += " " + item.rating + " Stars";

    return title;


}

function getCardButtons(item) {
    var buttons =  [{//for link to item
        buttonText:'BUY',
        buttonType:'url',
        target:item.url,
    }];

    if (item.rating) {
        buttons.push({//for recomendation
            buttonText:getReviewTitleFor(item),
            buttonType:'url',
            target:item.source == 'bestbuy' ? item.url : item.reviews,
        });
    }

    if (item.energyStar && item.energyStar == 'true') {
        buttons.push({
            buttonText:"Energy Star",
            buttonType:'url',
            target:item.source == 'bestbuy' ? item.url : item.reviews,
        });
    }

    return buttons;
}

module.exports = {buildMessageFor}