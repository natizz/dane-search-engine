'use strict';

const amazon = require('amazon-product-api');
const numeral  = require('numeral');
const config = require('./../../../config/config').engine.providers.amazon;

const client = amazon.createClient(config);

/**
 * API Docs:
 * http://docs.aws.amazon.com/AWSECommerceService/latest/DG/CHAP_OperationListAlphabetical.html
 * http://docs.aws.amazon.com/AWSECommerceService/latest/DG/ItemSearch.html
 */
const searchItemBase = {
  Availability: 'Available',
  SearchIndex: 'PCHardware',
  Condition: "New",
  Sort: "psrank",//reviewrank,reviewscore not all filter are availabel
  ResponseGroup: 'Large,ItemAttributes,Images,BrowseNodes'
};

// http://www.findbrowsenodes.com/us/PCHardware/13896617011
const categories = {
  laptop: '565108',
  desktop: '565098',
  tablet: '1232597011',
  phone: '2407748011'
};
const defaultCategory = categories['laptop'];

var requestCounter = 1;//a hack for throtteled netwrok requests

/**
 * Map features to search keywords that will be used in the search (instead of being as-is)
 */

function searchCategory({ category, params = {} }, callback) {
  const searchParams = Object.assign(
    {
      // Category
      BrowseNode: categories[category.toLowerCase()] || defaultCategory,

      // Price Range
      //Note, according to amazon API requests, we need to send 80000 for 800$ items so we are adding 00 in end of each price
      MinimumPrice: (params.features && params.features.priceRange && params.features.priceRange.min && params.features.priceRange.min + '00') || '1',
      MaximumPrice: (params.features && params.features.priceRange && params.features.priceRange.max && params.features.priceRange.max + '00') || '99999999',
      ItemPage: params.itemPage || 1,
      // Warranty?
      //TODO
      // Features
      Keywords: (params.features && params.features.keywords) || ''
    },
    searchItemBase
  );

  console.log('AMAZON Search params', searchParams);

  console.log(`Amazon: Running amazon search (params: ${JSON.stringify(searchParams)}`);
  client.itemSearch(searchParams, (err, results, response) => {
    if (err) {
      callback(err);
      console.log(err);
      return;
    }
    callback(null, results.map(normalize));
  });
}

function itenLookupPromise(item) {
  return new Promise((resolve, reject)=> {

    if (requestCounter >= 20) {
      requestCounter = 0;
    }
    requestCounter+=2;
    const timing = 1000 * requestCounter;
    setTimeout(() => {
      const asin = item.id.replace("amazon-", "");
      itemLookup(asin, (err, item)=> {
        if (err) {
          resolve({});
          return;
        }
        resolve(item);
      });

    }, timing);
  });
}


function itemLookup(asin, callback = () => {}) {
  client.itemLookup({
    idType: 'ASIN',
    itemId: asin,
    responseGroup: searchItemBase.ResponseGroup,
    Availability: 'Available',
  }, (err, results) => {
    if (err) {
      callback(err);
      console.log(err);
      return;
    }
    if (!results.length) {
      callback();
      console.log(`amazon: ${asin} not found.`);
      return;
    }
    callback(null, normalize(results[0]));
  })
}

function normalize(item) {
  const itemDetails = item && item.ItemAttributes[0];

  // let hdd = extract_hdd_size_and_type(itemDetails.Feature);

  return {
    id: `amazon-${item.ASIN && item.ASIN[0]}`,
    url: item.DetailPageURL && item.DetailPageURL[0],
    image: item.LargeImage && item.LargeImage[0].URL[0],
    title: itemDetails.Title && itemDetails.Title[0],
    screenSize: extract_screen_size(itemDetails.Feature.concat(itemDetails.Title)),//hack, sometimes screen size is in title
    price:
      itemDetails.ListPrice &&
      itemDetails.ListPrice[0] &&
      itemDetails.ListPrice[0].FormattedPrice[0],
    esPrice:
      itemDetails.ListPrice &&
      itemDetails.ListPrice[0] &&
      itemDetails.ListPrice[0].FormattedPrice[0] && numeral(itemDetails.ListPrice[0].FormattedPrice[0]).value(),
    brand: itemDetails.Brand && itemDetails.Brand.join(', '),
    color: itemDetails.Color && itemDetails.Color.join(', '),
    features: itemDetails.Feature,
    productGroup: itemDetails.ProductGroup && itemDetails.ProductGroup[0] || '',
    productType: itemDetails.ProductTypeName && itemDetails.ProductTypeName[0],
    dimensions: itemDetails.ItemDimensions && itemDetails.ItemDimensions[0],
    os: itemDetails.OperatingSystem && itemDetails.OperatingSystem[0],
    manufacturer:
      itemDetails.Manufacturer && itemDetails.Manufacturer.join(', '),
    warranty: itemDetails.Warranty && itemDetails.Warranty.join(', '),
    partNumber: itemDetails.PartNumber && itemDetails.PartNumber.join(', '),
    productId: item.ASIN && item.ASIN[0],
    reviews: extract_reviews_link(item),
    rating:item.customerReviewAverage,
    available:true,
    // hdd:hdd.size,
    // hddType:hdd.type,
    energyStar:shouldHaveEnergyStay(itemDetails.Feature.concat(itemDetails.Title)),
    source: 'amazon'
  };
}

function shouldHaveEnergyStay(features) {
  let shouldHaveStar = 'false';
  if (!features || features.length == 0) {
      return shouldHaveStar;
  }

  features.forEach((f) => {
      let f_str = f.toLowerCase();
      if (f_str.includes('battery')) {
        //Extracting suspect hours
        let hours = numeral(f_str.replace(/[^0-9\.]+/g,"")).value();
        if (hours > 6) {
          shouldHaveStar = 'true';
        }
      }
  });

  return shouldHaveStar;
}

function extract_hdd_size_and_type(features) {
  let type = 'unknown', size = 'unknown';
  if (!features || features.length == 0) {
    return {type:type, size:size};
  }

  for (var i = 0; i<features.length; i+=1) {
    const single_feature = features[i].toLowerCase();
    if (single_feature.includes('hdd')) {
      type = 'hdd';
    }
    if (single_feature.includes('hard drive')) {
      type = 'hard drive';
    }
    if (single_feature.includes('ssd')) {
      type = 'ssd';
    }
    if (type == 'unknown') {
      continue;
    }
    const idx = single_feature.indexOf(type);
    if (idx == -1) {
      continue;
    }
    const hdd_wrap = single_feature.slice(Math.max(0, idx-20), Math.min(single_feature.length, idx+20));
    if (hdd_wrap.includes('tb')) {
      size = hdd_wrap.replace(/[^0-9\.]+/g,"");
      size = numeral(size*1000).value();
      size = size.toString();
    }
    if (hdd_wrap.includes('gb')) {
      size = hdd_wrap.replace(/[^0-9\.]+/g,"");
    }
    break;
  }
  return {type:type, size:size};
}

function extract_reviews_link(item) {
  let review_link = '';
  if (!item.ItemLinks || item.ItemLinks.length == 0) {
    return review_link;
  }
  const link_array = item.ItemLinks[0].ItemLink;
  link_array.forEach((link_obj) => {
      if (link_obj.Description.length > 0 && link_obj.Description[0].includes('Reviews')) {
          review_link = link_obj.URL[0];
      }
  });
  return review_link;
}

//TODO - improve function to use wit.AI for NLP diagnosis for screen size
function extract_screen_size(raw_data_array) {
  var screenSize = '';
  if (!raw_data_array || raw_data_array.length == 0) {
    return screenSize;
  }
  raw_data_array.some((d) => {
      const lower_d = d.toLowerCase();
      if (lower_d.includes('screen') || lower_d.includes('inch')) {
        //We have a hit on potential screen size string
        screenSize = lower_d.replace(/[^0-9\.]+/g,"");
        return true;
      }
  });
  return screenSize;
}

module.exports = { searchCategory, itemLookup, itenLookupPromise };
