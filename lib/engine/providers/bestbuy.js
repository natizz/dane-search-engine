'use strict';

const config = require('./../../../config/config').engine.providers.bestbuy;
const bby = require('bestbuy')(config.key);

const normalizeRating = require('./../../scrapers/productsNormalizer').normalize;

const numeral  = require('numeral');
// https://bestbuyapis.github.io/api-documentation/?javascript#products-api
const categories = {
  desktop: 'abcat0501000',
  laptop: 'abcat0502000',
  tablet: 'pcmcat209000050006'
};
const defaultCategory = categories['laptop'];
var requestCounter = 1;
/**
 * Map features to search keywords that will be used in the search (instead of being as-is)
 */
const featuresKeywordsMapper = {
    screenSize: (k) => `${k} inch`,
}

function singleProductSearch(sku) {

  return new Promise((resolve, reject) => {

    if (requestCounter >= 20) {
      requestCounter = 0;
    }
    requestCounter+=2;
    const timing = 1000 * requestCounter;

    setTimeout(() => {
      bby.products(sku, {"show":"sku,name,salePrice,onlineAvailability"})
          .then((data) => {
            resolve(data);
          })
          .catch((err) => {
            console.warn(err);
            reject();
          });
    },timing);
  });
}


function searchCategory({ category, params }, callback) {
  const searchParamsObj = {
    condition: 'new',
    // name: `${Object.keys(params.features || {})
    //   .map(k => featuresKeywordsMapper[k] ? featuresKeywordsMapper[k](params.features[k]) : params.features[k])
    //   .join(' ')}*`,
    'categoryPath.id': categories[category] || defaultCategory,
    // page: params.itemPage || 1,
  };

  let searchParams = Object.keys(searchParamsObj)
    .map(k => `${k}=${searchParamsObj[k]}`)
    .join('&');

  if (params.features.priceRange) {
    if (params.features.priceRange.min) {
      searchParams += `&salePrice>=${params.features.priceRange.min}`;
    }
    if (params.features.priceRange.max) {
      searchParams += `&salePrice<=${params.features.priceRange.max}`;
    }
  }

  console.log(`BestBuy: Running bestbuy search (params: ${JSON.stringify(searchParams)}`);

  bby.products(`${searchParams}`,
    {
      show: 'all',
      pageSize:15,
      page:params.itemPage || 1
    },
    (err, results) => {
      if (err) {
        callback(err);
        return;
      }
      //filters none available products
      const res = results.products.filter((p) => {
          if (p.onlineAvailability) {
            return true;
          } else {
            return false;
          }
      });

      callback(null, res.map(normalize));
    }
  );
}

function normalize(item) {

  const normRating = normalizeRating(item.customerReviewAverage, item.customerReviewCount);

  return {
    id: `bestbuy-${item.productId}`,
    url: item.url,
    image: item.image,
    title: item.name,
    price: item.salePrice,
    esPrice: item.salePrice && numeral(item.salePrice).value(),
    regPrice:item.regularPrice,
    brand: item.manufacturer,
    color: item.color,
    features: item.features.map(f => f.feature || ''),
    productGroup: '',
    productType: item.type,
    dimensions: { height: item.height, width: item.width, depth: item.depth },
    os: item.mobileOperatingSystem,
    manufacturer: item.manufacturer,
    warranty: item.warrantyLabor ? `Labor: ${item.warrantyLabor} / Parts: ${item.warrantyParts}` : null,
    partNumber: item.number,
    productId: item.productId,
    screenSize: item.screenSizeIn ? item.screenSizeIn.toString() : extract_screen_size(item.details),
    sku:item.sku,
    weight:item.weight,
    ram:extract_ram_size(item.details),
    hdd: item.driveCapacityGb ? item.driveCapacityGb:extract_hdd_size(item.details),
    hddType:extract_hdd_size_and_type(item.features).type,
    energyStar:shouldHaveEnergyStay(item.details),
    rating:item.customerReviewAverage,
    reviewsCount: item.customerReviewCount,
    wisdom: normRating > 0.8 ? 'crowd' : undefined,
    available:true,
    source: 'bestbuy'
  };
}

function shouldHaveEnergyStay(features) {
  let shouldHaveStar = 'false';
  if (!features || features.length == 0) {
    return shouldHaveStar;
  }

  features.forEach((d) => {
    let key = d.name.toLowerCase();
    let val = d.value.toLowerCase();
    if (key.includes('battery')) {
      //Extracting suspect hours
      let hours = numeral(val.replace(/[^0-9\.]+/g,"")).value();
      if (hours > 6) {
        shouldHaveStar = 'true';
      }
    }
  });

  return shouldHaveStar;
}

function extract_hdd_size_and_type(features) {
  let type = 'unknown', size = 'unknown';
  if (!features || features.length == 0) {
    return {type:type, size:size};
  }

  for (var i = 0; i<features.length; i+=1) {
    const single_feature = features[i].feature.toLowerCase();
    if (single_feature.includes('hdd')) {
      type = 'hdd';
    }
    if (single_feature.includes('ssd')) {
      type = 'ssd';
    }
    if (type == 'unknown') {
      continue;
    }
    const idx = single_feature.indexOf(type);
    if (idx == -1) {
      continue;
    }
    const hdd_wrap = single_feature.slice(Math.max(0, idx-7), Math.min(single_feature.length, idx+7));
    if (hdd_wrap.includes('tb')) {
      size = hdd_wrap.replace(/[^0-9\.]+/g,"");
      size = numeral(size*1000).value();
      size = size.toString();
    }
    if (hdd_wrap.includes('gb')) {
      size = hdd_wrap.replace(/[^0-9\.]+/g,"");
    }
    break;
  }
  return {type:type, size:size};
}

function extract_screen_size(raw_data_array) {
  var screenSize = '';
  if (!raw_data_array || raw_data_array.length == 0) {
    return screenSize;
  }
  raw_data_array.some((d) => {
      if (d.name && d.name.toLowerCase().includes('screen size')) {
          screenSize = d.value.toLowerCase().replace(/[^0-9\.]+/g,"");
          return true;
      }
  });
  return screenSize;
}

function extract_ram_size(raw_data_array) {
  var ramSize = '';
  if (!raw_data_array || raw_data_array.length == 0) {
    return ramSize;
  }
  raw_data_array.some((d) => {
    if (d.name && d.name.toLowerCase().includes('system memory (ram)')) {
      ramSize = d.value.toLowerCase();
      return true;
    }
  });
  return ramSize;
}

function extract_hdd_size(raw_data_array) {
  var hddSize = '';
  if (!raw_data_array || raw_data_array.length == 0) {
    return hddSize;
  }
  raw_data_array.some((d) => {
    if (d.name && d.name.toLowerCase().includes('hard drive capacity')) {
      hddSize = d.value.toLowerCase();
      return true;
    }
});
  return hddSize;
}
module.exports = { searchCategory, singleProductSearch };
