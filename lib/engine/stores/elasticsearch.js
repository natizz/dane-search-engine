'use strict';

const elasticsearch = require('elasticsearch');
const config = require('./../../../config/config').engine.stores.elasticsearch;
const _ = require('lodash');
const qBuilder = require("./esQueryBuilder");


const client = new elasticsearch.Client({
    host: config.url,
    log: 'error'
});

function ping(callback = () => {}) {
    client.ping({ requestTimeout: 30000 }, callback);
}

//used to iterrate over all db
function scrollRaw(query, getMoreUntilDone) {
    return client.scroll(query, getMoreUntilDone);
}


function productsCount(cb) {
    client.count({index: 'products'},(err, res) => {
        if (err) {
            //TODO error handling
            console.log('err');
            if (cb) cb(null, err);
            return;
        }
        if (cb) cb(res.count);
    })
}

function searchRaw(q, callback = () => {}) {
    return client.search(q, callback);

}

function search(category, params) {
    return new Promise((resolve, reject) => {
        const query = qBuilder.buildQuesryBy(category, params);
        client.search(query).then((res) => {
            if (!res ||  !res.hits){
                reject(new Error("ES, search ended successfully, with not results"));
                return;
            }
            const hits = res.hits;
            if (!hits.hits || hits.hits.length == 0 ) {
                reject(new Error("ES, search ended successfully, with not results"));
                return;
            }
            const products = hits.hits.map( (obj) => obj._source);
            resolve(products);
        }, (err) => {
            reject(err);
        });
    });
}

function updateResults(items, category, callback = () => {}) {
  const body = [];
  items.forEach(item => {
    body.push({
      update: {
        _index: 'products',
        _type: category,
        _id: item.id,
      },
    });
    body.push({doc: item});
  });

  client.bulk({body}, (err, res) => {
    if (err) {
      console.log(`Error updating results to ES: ${err}`);
      callback(err);
      return;
    }
    let errorCount = 0;
    res.items.forEach(item => {
      if (item.index && item.index.error) {
        console.log(++errorCount, item.index.error);
      }
    });
    console.log(`Successfully indexed ${items.length - errorCount} out of ${items.length} items`);
  });
}

function saveResults(items, category, callback = () => {}) {
    const body = [];
    items.forEach(item => {
        body.push({
            index: {
                _index: 'products',
                _type: category,
                _id: item.id,
            },
        });
        body.push(item);
    });

    client.bulk({body}, (err, res) => {
        if (err) {
            console.log(`Error saving results to ES: ${err}`);
            callback(err);
            return;
        }
        let errorCount = 0;
        res.items.forEach(item => {
          if (item.index && item.index.error) {
            console.log(++errorCount, item.index.error);
          }
        });
        console.log(`Successfully indexed ${items.length - errorCount} out of ${items.length} items`);
        callback();
    });

}

module.exports = { ping, saveResults, updateResults, productsCount, search, searchRaw, scrollRaw};