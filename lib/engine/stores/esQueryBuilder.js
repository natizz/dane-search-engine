/**
 * Created by danielbachar on 24/08/2017.
 */

const bodyBuilder = require("bodybuilder");
const _ = require('lodash');

const search_params = { //Example
    ram_range:{
        max:'128',
        min:'8'
    },
    cpu:[],
    screenSize:[15,16,17],
    priceRange:{
        max:99999,
        min:800
    },
    warranty:'true',
};

function buildQuesryBy(category, searchParams) {


    var params = {index: 'products', type:category || 'laptop'};
    var bBuilder = bodyBuilder();
    //TODO add weights to the search
    //TODO - adding warranty
    bBuilder.andFilter('match', 'available', 'true');

    if (searchParams.energyStar) {
        bBuilder.andFilter('match', 'energyStar', 'true');
    }
    if (searchParams.os && searchParams.os.length > 0) {//OS filter
        bBuilder.andFilter('match', 'os', searchParams.os);
    }
    if (searchParams.screenSize && searchParams.screenSize.length > 0) {//Screen Filter
        searchParams.screenSize.forEach((s_size) => {
            const scs = Math.floor(s_size);
            bBuilder.orFilter('range', 'screenSize', {
                lte:scs+0.99,//max
                gte:scs//min
            });
            // bBuilder.orFilter('match', 'screenSize', s_size);
        });
    }
    if (searchParams.hddSize) {
        bBuilder.andFilter('range', 'hdd', {
            lte:searchParams.hddSize,//max
            gte:0//min
        });
    }
    if (searchParams.hddType) {
        bBuilder.andFilter('match', 'hddType',searchParams.hddType );
    }
    if (searchParams.priceRange) {
        bBuilder.andFilter('range', 'esPrice', {
            lte:searchParams.priceRange.max,//max
            gte:searchParams.priceRange.min//min
        });
    }
    if (searchParams.brands && searchParams.brands.length > 0) {

        // bBuilder.andFilter('nested', 'path', 'prop1', (q) => {//TODO - nested
        //    return q.orFilter()
        // });
        var locBuilder = bodyBuilder();
        searchParams.brands.forEach((brand) => {
            locBuilder.orFilter('match', 'brand', brand);
            // locBuilder.orFilter('match', 'manufacturer', brand);
            // locBuilder.orFilter('match', 'brand', capitalizeFirstLetter(brand));
            // locBuilder.orFilter('match', 'manufacturer', capitalizeFirstLetter(brand));
        });

        // const bool = bBuilder.getFilter().bool;
        // if (Array.isArray(bool.must)) {
        //     //Handle array - push
        //     const filter = locBuilder.getFilter();
        //     bool.must.push(filter);
        // } else {
        //     //take current and tunr ino array, query should work
        //     const filter = [_.assign({},bool.must), locBuilder.getFilter()]
        //     bool.must = filter;
        //     // bool.must.push(locBuilder.getFilter());
        // }
        bBuilder.andFilter('match', 'brand', searchParams.brands[0]);
    }

    //aggrigation by count

    //Max size

    //Must have rating field
    bBuilder.filter('exists','field','rating');

    //Adding sort by our ranking
    bBuilder.sort('normalizedRating', "desc");
    bBuilder.sort('esPrice', "asc");
    //TODO - add aggrigaiton between groups of ratings - i.e. 1/2/3/4/5, then apply sort by esPrice between groups and take lowest price btween each group

    params.body =  bBuilder.build();
    return params;
}

//Helper
function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

//For Tesing

// setTimeout(() => {
//     var params = {index: 'products', type:'laptop'};
//
//     params.body = {
//         query: {
//             bool: {
//                 must: { "match_all": {} },
//                 filter: {
//
//                 }
//             }
//         }
//     };
//
//     //
//     // params.body = {
//     //         query: {
//     //         bool: {
//     //             must: {
//     //                 "multi_match": {
//     //                     "operator": "and",
//     //                         "fields": [
//     //                         "os",
//     //                         // "title",
//     //                         // "publisher",
//     //                         // "year"
//     //                     ],
//     //                         "query": "mac"//"George Orwell"
//     //                 }
//     //             },
//     //             // "filter": {
//     //             //     "terms": {
//     //             //         "year": [
//     //             //             1980,
//     //             //             1981
//     //             //         ]
//     //             //     }
//     //             // }
//     //         }
//     //     }
//     // };
//     var body = queryBuilder();
//
//     // body.filter('term', 'os', 'windows');
//     // body.orFilter('term', 'brand', 'dell');
//     // body.orFilter('term', 'brand', 'apple');
//
//     body.andFilter('range', 'price', {
//         lte:1000,//max
//         gte:500//min
//     });
//
//     params.body = body.build();
//
//     // params.body = queryBuilder()
//     //     .query('match', 'os', 'windows')
//     //     .query('match', 'brand', 'dell')
//     //     .build();
//
//     es.search(params).then((res) => {
//         console.log(res);
//     }, (err) => {
//         console.log(err);
//     });
// }, 5000);

module.exports = { buildQuesryBy };