'use strict';

const async = require('async');
const numeral  = require('numeral');
const _		 = require("lodash");

const fristBy =  require('thenby');
const es = require('./stores/elasticsearch');

const amazon = require('./providers/amazon');
const bestbuy = require('./providers/bestbuy');
const SearchError = require('./../util/SearchError');
const mBuilder = require('../util/messageBuilder');

const categories = ['laptop', 'tablet'];

function getSupportedCategories() {
    return categories;
}

function searchCategoryPromise({ category, params = {} }) {
    return new Promise((resolve, reject) => {
        searchCategoryProvider({ category, params }, (err, results) => {
            if (err) {
                reject(err);
                return;
            }
            resolve(results);
        });

    }, (err) => {
        reject(err);
    });

}

function searchCategoryProvider({ category, params = {} }, callback) {
    if (!categories.includes(category)) {
        callback(new SearchError(400, `Invalid category: ${category}.`));
        return;
    }
    if (!params.features) {
        callback(new SearchError(400, `No features provided.`));
        return;
    }

    async.applyEach(
        [amazon.searchCategory, bestbuy.searchCategory],
        { category, params },
        (err, results) => {
            if (err) {
                callback(err);
                return;
            }

            const notFilterdItems = results.reduce((a, b) => a.concat(b), []);
            //Saving to es
            es.saveResults(notFilterdItems, category);

            var items = [];
            //Manual filtering, to be rplaced by es earch(category, params)
            notFilterdItems.forEach((item) => {
                if (complyToFilter(params.features, item)) {
                    items.push(item);
                }
                if (!item.title) {
                    console.log('crack, no title for item');
                }
            });

            const sortItems = items.sort(
                fristBy((a, b) => {
                    if (a.rating && b.rating) {
                        return b.rating - a.rating;
                    }
                    if (a.rating) {
                        return -1;
                    }
                    if (b.rating) {
                        return 1;
                    }
                    return 0;//equal
                }).thenBy((a, b)=> {
                    if (a.esPrice && b.esPrice) {
                        return b.esPrice - a.esPrice;
                    }
                    if (a.esPrice) {
                        return 1;
                    }
                    if (b.esPrice) {
                        return -1;
                    }
                    return 0;//equal
                })
            );

            callback(null, mapItemToCards(sortItems));
        }
    );
}

function searchCategory({ category, params = {} }, callback) {
    if (!categories.includes(category)) {
        callback(new SearchError(400, `Invalid category: ${category}.`));
        return;
    }
    if (!params.features) {
        callback(new SearchError(400, `No features provided.`));
        return;
    }

    //Hack to prevent ES From been over use by cron jobs
    if (params.features.shouldNOTSearch === 'true') {
        if (callback) callback("I'm up!, Don't worry");
        return;
    }

    es.search(category, params.features).then((res) => {
        callback(null, mapItemToCards(res));
    }, (err) => {
        console.log('ES Search Filed with Error - ', err);
        //Fall back to old search
        searchCategoryProvider({ category, params }, callback);
    });
}

function complyToFilter(filterBy, item) {

    //Most of the data we need is in res.features which is an array of features
    const p = item;
    if (!p || !filterBy) {
        return false;
    }

    var isComply = true;
    _.forIn(filterBy, (val, key) => {
        //Early break
        if (!isComply) {
            return false;
        }
        switch (key) {
            case 'priceRange': //verifier, search shold already return relevant prices only
                const pPrice = numeral(p.price).value();
                if (val && (val.min || val.max) && pPrice) {
                    if (val.min > pPrice || val.max < pPrice) {
                        isComply = false;
                        console.log('Price compliment problem, should not happen unless we encounter API pproblem');
                    }
                }
                break;
            case 'os':
                let pOS = p.os;
                if (Array.isArray(val) && val.length && pOS) {
                    pOS = pOS.toLowerCase();
                    let locComply = true;
                    val.every( (os_type) => {
                        locComply =  pOS.includes(os_type.toLowerCase());
                        return !locComply;//will stop the loop if locComply = true,
                    });
                    isComply = isComply && locComply;
                }
                break;
            case 'warranty':
                let hasWar = p.warranty;
                if (val != null && hasWar) {
                    isComply = isComply && val;
                }
                break;
            case 'screenSize':
                if (val != null) {
                    let locComply = false;
                    (Array.isArray(val) ? val : [val]).some((s_size) => {
                        return locComply = p.screenSize.includes(s_size);
                    });
                    isComply = isComply && locComply;
                }
                break;
            case 'brands':
                if (val.indexOf(p.brand.toLowerCase()) == -1) {
                    isComply = false;
                }
                break;
            case 'ramRange':
                break;
            case 'hddType':
                break;
            case 'hddSize':
                break;

        }
    });
    return isComply;
}

function mapItemToCards(items) {
    var cards = [];
    if (!items || !Array.isArray(items)) {
        return cards;
    }

    return mBuilder.buildMessageFor(items);


}



module.exports = { searchCategory , getSupportedCategories, searchCategoryPromise};
