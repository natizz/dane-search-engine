/**
 * Created by danielbachar on 24/08/2017.
 */


const engine = require("../engine/engine");
const search = require("./searchProvider");
const laptopmag = require('./providers/laptopmag');
const async = require("async");
const queue = require('queue');

const q = queue({concurrency:1,autostart:false});
//TODO:
//1) build a queue for jobs
//2) configure update job

function insertToQueue(page, categories, params) {
    q.push((cb) => {
        //Adding timeout for slower scraping, amazon limit every 1 min
        console.log('Updating page - ', page);
        setTimeout(()=> {
            params.itemPage = page;
            var promises = [];
            categories.forEach((category)=>{
                const p = engine.searchCategoryPromise({category, params});
                promises.push(p);
            });
            Promise.all(promises).then((res) => {
                if (cb) cb();
            }, (err) => {
                //TODO error hanlidng
                if (cb) cb();
            });
        }, page * 60000);
    });
}

function run(cb) {
    const categories = engine.getSupportedCategories();
    var params = search.generatSearchParams();

    for (var i = 1; i < 10; i++) {
        insertToQueue(i, categories, params);
    }
    q.push((cb) => {
        setTimeout(() => laptopmag.scrapeAndSave(), 10000);
    });
    q.start();
    q.on('end', (err) => {
        console.log('Product scraper q run ended');
        if (cb) cb;
    });
}

module.exports = { run };