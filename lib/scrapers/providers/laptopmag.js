const cheerio = require('cheerio');
const async = require('async');
const querystring = require('querystring');
const request = require('requestretry').defaults({
    headers: {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.101 Safari/537.36',
        'Cookie': 'RAAS=Device%3D%3ECOMPUTER%3A%3ACountry%3D%3EIL; cm_4894=1; __uzma=59a04b747eae60.14955243; __uzmb=1503677300; PHPSESSID=rjccjnmntdd04sk6o0njorpkj2; __ssds=2; __qca=P0-1312909892-1503677322706; newsletterpopup=true; optimizelyEndUserId=oeu1503677333124r0.974697508666805; optimizelySegments=%7B%222018642089%22%3A%22gc%22%2C%222031840458%22%3A%22direct%22%2C%222032520751%22%3A%22false%22%7D; optimizelyBuckets=%7B%7D; __unam=30f307c-15e1a27106a-72417de7-60; __uzmc=3842111299044; __uzmd=1503681539; __utmt=1; __utma=138595628.1853784217.1503677309.1503677841.1503681548.5; __utmb=138595628.1.10.1503681548; __utmc=138595628; __utmz=138595628.1503677841.4.4.utmcsr=feedburner|utmccn=Feed:%20laptopmag%20(LAPTOP%20Mag%20Main%20Feed)|utmcmd=feed; cm_4894=1; _ga=GA1.2.1853784217.1503677309; _gid=GA1.2.1816203076.1503677310; __ssuzjsr2=a9be0cd8e; _parsely_session={%22sid%22:1%2C%22surl%22:%22https://www.laptopmag.com/articles/subscribe-rss-feeds-safari%22%2C%22sref%22:%22https://www.google.co.il/%22%2C%22sts%22:1503677324880%2C%22slts%22:0}; _parsely_visitor={%22id%22:%22e3387731-2109-4031-a06e-f916c6645942%22%2C%22session_count%22:1%2C%22last_session_ts%22:1503677324880}',
    },
});

const amazon = require('./../../engine/providers/amazon');
const es = require('./../../engine/stores/elasticsearch');

const baseURL = 'https://www.laptopmag.com/';

function scrapeAndSave(callback = () => {}) {
    scrape((err, res) => {
        if (err) {
            console.log('Error scraping laptopmag', err);
            callback(err);
            return;
        }
        es.saveResults(res, 'laptop');
    })
}

function scrape(callback = () => {}) {
    request.get(baseURL, (err, res, body) => {
        if (err) return callback(err);

        const $ = cheerio.load(body);
        const cats = new Set();
        $('.catSubItem').each((i ,el) => {
            const url = $(el).find('a').attr('href');
            if (url && url.includes('laptops') && url.split('/').length === 4 && !url.includes('search')) {
                cats.add(url);
            }
        });

        console.log(`Scraping cateogires:`, cats);

        async.concat(cats, scrapeCategory, callback);
    });
}

function scrapeCategory(url, callback) {
    request.get(url, (err, res, body) => {
        if (err) return callback(err);

        const $ = cheerio.load(body);
        const items = [];
        $('.item-content').each((i, el) => {
            const item = { review: {} };

            // Link and name
            const itemReviewLink = $(el).find('.item-name a');
            item.review.link = itemReviewLink.attr('href');
            item.review.name = itemReviewLink.text();
            
            // Url
            const itemBuyUrl = $(el).find('a.buyBtn');
            item.merchant = (itemBuyUrl.attr('data-merchant') || '').toLowerCase().replace(/\s/g, '');
            item.url = extractItemBuyUrl(itemBuyUrl.attr('href'), item.merchant);

            // Rating
            try {
                const classes = $(el).find('.rating').attr('class').split(' ');
                classes.forEach(cl => {
                    if (cl.startsWith('stars-')) {
                        item.review.rating = parseInt(cl.split('-')[1], 10);
                    }
                });
            } catch (e) {}

            if (item.url != null) {
                items.push(item);
            }
        });
        async.concat(items.filter(item => item.merchant === 'amazon'), fetchItem, callback);
    });
}

function fetchItem(item, callback) {
    if (item.merchant == 'amazon') {
        const url = item.url;
        amazon.itemLookup(url.substr(url.lastIndexOf('/') + 1), (err, res) => {
            if (err) {
                return;
            }
            res.review = item.review;
            callback(null, res);
        });
    } else {
        callback(new Error("Not supported"));
    }
}
 
function extractItemBuyUrl(url, merchant, callback = () => {}) {
    let res = null;
    if (url.includes('geni.us') || url.includes('purch.com')) { // reseller redirect link
        // request.get({url, followAllRedirects: true}, (err, response, body) => {
        //     callback(response && response.request && response.request.uri || url);
        // });
        // return;
    }
    else if (url.includes('=https%3A%2F%2') || url.includes('=http%3A%2F%2')) { // reseller direct link
        const params = querystring.parse(url.substr(url.indexOf('?')));
        Object.keys(params).forEach((k) => {
            const v = params[k];
            if (v.startsWith('http')) {
                res = v;
            }
        });
    } else { // assume direct link
        res = url.split('?')[0];
    }
    return res;
}

module.exports = { scrape, scrapeAndSave };