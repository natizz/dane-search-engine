const cheerio = require('cheerio');
const request = require('requestretry').defaults({
    headers: {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.101 Safari/537.36',
        'Cookie': 'RAAS=Device%3D%3ECOMPUTER%3A%3ACountry%3D%3EIL; cm_4894=1; __uzma=59a04b747eae60.14955243; __uzmb=1503677300; PHPSESSID=rjccjnmntdd04sk6o0njorpkj2; __ssds=2; __qca=P0-1312909892-1503677322706; newsletterpopup=true; optimizelyEndUserId=oeu1503677333124r0.974697508666805; optimizelySegments=%7B%222018642089%22%3A%22gc%22%2C%222031840458%22%3A%22direct%22%2C%222032520751%22%3A%22false%22%7D; optimizelyBuckets=%7B%7D; __unam=30f307c-15e1a27106a-72417de7-60; __uzmc=3842111299044; __uzmd=1503681539; __utmt=1; __utma=138595628.1853784217.1503677309.1503677841.1503681548.5; __utmb=138595628.1.10.1503681548; __utmc=138595628; __utmz=138595628.1503677841.4.4.utmcsr=feedburner|utmccn=Feed:%20laptopmag%20(LAPTOP%20Mag%20Main%20Feed)|utmcmd=feed; cm_4894=1; _ga=GA1.2.1853784217.1503677309; _gid=GA1.2.1816203076.1503677310; __ssuzjsr2=a9be0cd8e; _parsely_session={%22sid%22:1%2C%22surl%22:%22https://www.laptopmag.com/articles/subscribe-rss-feeds-safari%22%2C%22sref%22:%22https://www.google.co.il/%22%2C%22sts%22:1503677324880%2C%22slts%22:0}; _parsely_visitor={%22id%22:%22e3387731-2109-4031-a06e-f916c6645942%22%2C%22session_count%22:1%2C%22last_session_ts%22:1503677324880}',
    },
});

const normalizeRating = require('./../../scrapers/productsNormalizer').normalize;


function getReviews(url, callback = () => {}) {
    request.get(url, (err, response, body) => {
        if (err) return callback(err);
        
        const $ = cheerio.load(body);

        const res = {};

        res.rating = parseFloat($('[data-hook="average-star-rating"] span').text().split(' ')[0]);
        res.reviewsCount = parseInt($('[data-hook="total-review-count"]').text().replace(',', ''), 10);

        res.normalizedRating = normalizeRating(res.rating, res.reviewsCount);

        res.wisdom = res.normalizedRating > 0.8 ? 'crowd' : undefined;

        callback(null, res);
    })
}

module.exports = { getReviews };