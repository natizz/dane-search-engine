/**
 * Created by danielbachar on 04/09/2017.
 */
const es = require('./../engine/stores/elasticsearch');
const engine = require('./../engine/engine');

function run() {
  const categories = engine.getSupportedCategories();
  categories.forEach((category,i) => {
    setTimeout(() => doNormalize(category),100 + (i * 5000));
  });

}


function doNormalize(category,callback = () => {}) {
  es.searchRaw({
    index: 'products',
    type: category,
    size: 10000,
    body: {
      query: {
        bool: {
          must: [{
            exists: {
              field: 'reviewsCount',
            }
          },
            {
              exists: {
                field: 'rating',
              }
            }
          ],
          must_not: {
            exists: {
              field: 'normalizedRating'
            }
          }
        },
      }
    }
  },(err,res) => {
    if (err) {
      console.log(`Error searching`, err);
      callback(err);
      return;
    }
    console.log(`Found ${res && res.hits && res.hits.hits.length} results, normalizing...`);

    const products = res && res.hits && res.hits.hits.map(item => applyFormula(item._source));

    console.log("RES", JSON.stringify(products));

    es.updateResults(products, category, (err,updateRes) => {
      if (err) {
        console.log(`Error saving normalized ratings`);
        callback(err);
      } else {
        console.log(`Successfully normalized ratings`, updateRes);
      }
    })
  });
}


function applyFormula(item) {
  const res = { id: item.id };
  if (item.rating === null|| item.reviewsCount === null) {
    return res;
  }
  res.normalizedRating = normalize(item.rating,item.reviewsCount);
  if (item.normalizedRating > 0.8) {
    res.wisdom = 'crowd';
  }
  return res;
}

function normalize(rating,reviewsCount) {
  rating = parseFloat(rating);
  reviewsCount = parseInt(reviewsCount, 10);
  return (Math.max(Math.min(1,(reviewsCount / 700)),0.5) * rating );
}

module.exports = {run, normalize};