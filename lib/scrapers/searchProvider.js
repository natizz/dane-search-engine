/**
 * Created by danielbachar on 24/08/2017.
 */

//Searach Params Example
/*
const search_params = {
    ram_range:{
        max:'128',
        min:'8'
    },
    cpu:[],
    screenSize:[15,16,17],
    priceRange:{
        max:99999,
        min:800
    },
    warranty:'true',
};
*/
const _ = require("lodash");

function getRandBetween(min, max) {
    return Math.random() * (max - min) + min;
}


function getRamRange() {

    // var range = ['4', '6', '8', '16', '32', '64', '128', '256'];
    //
    // return {
    //     max:,
    //     min:
    // }
}

function generatSearchParams() {
    return {
        features: {
            // ram_range:{
            //     max:'256',
            //     min:'2'
            // },
            // cpu:[],
            // screenSize:[7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24],
            priceRange:{
                max:999999,
                min:1
            },
            warranty:'true',
        }
    };
}


module.exports = { generatSearchParams };