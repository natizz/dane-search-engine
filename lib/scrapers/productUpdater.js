/**
 * Created by danielbachar on 30/09/2017.
 */


const engine = require("../engine/engine");

const queue = require('queue');
const es = require('./../engine/stores/elasticsearch');

const bestbuy = require('../engine/providers/bestbuy');
const amazon = require('../engine/providers/amazon');

const q = queue({concurrency:1,autostart:false});

const singleUpdateQueue = queue({concurrency:1,autostart:false});

function run(cb) {
    const categories = engine.getSupportedCategories();
    update(categories).then((res) => {
        if (cb) cb(res);
    }, (err) => {
        if (cb) cb(err);
    });

}

function update(categories) {
    return new Promise((resolve, reject) => {
        if (!categories || categories.length === 0) {
            reject(new Error('Missing Categories'));
            return;
        }
        categories.forEach((category) => {
            updateSingleCategory(category);
        });

        q.start();
        q.on('end', (err) => {
            console.log('Product scraper q run ended');
            resolve();
        });
    });


}

function updateSingleCategory(category) {

    q.push((cb) => {
        setTimeout(() => {

            es.searchRaw({
                index: 'products',
                type: category,
                scroll: '30s',
                body: {
                    query: {
                        bool: {
                            must: {
                                exists: {
                                    field: "reviewsCount",
                                }
                            }
                        }
                    }
                }
            }, (err,res) => {
                if (err) {
                    //TODO - error handling
                    if (cb) cb();
                    return;
                }
                //TODO - update hits
                const products = res.hits.hits.map(item => item._source);
                updateProducts(products, category, (err, ers) => {
                    scrollRecurse(res._scroll_id, category, false, (scroll_id) => {
                        if (cb) cb();
                    });
                })
            });

        }, 3000)
    });

}

function updateProducts(products, category, cb) {

    var promises = [];
    products.some((p)=>{
        if (p.sku) { //bby
            console.log('bestbuy single product update');
            promises.push(bestbuy.singleProductSearch(p.sku));
        }
        if (p.source && p.source === 'amazon') { //amazon
            console.log('amazon single product update');
            promises.push(amazon.itenLookupPromise(p))
        }
    });

    // promises.forEach((promise)=>{
    //     singleUpdateQueue.push((cb)=>{
    //         promise.then((res)=>{cb(res)}, (err)=>{cb()}).catch((reason)=>{cb()})
    //     });
    // });
    //
    // singleUpdateQueue.start()
    // singleUpdateQueue.on('end', (err) => {
    //     console.log('Product scraper q run ended');
    //     resolve();
    // });

    if (promises.length === 0) {
        cb();
        return;
    }

    Promise.all(promises).then((res) => {

        //TODO - improve - super not efficient im ashame ):
        products.forEach((p)=>{
            res.forEach((productVerified)=>{
                if (p.sku === productVerified.sku) {//bestbuy
                    p.available = productVerified.onlineAvailability;
                } else if (p.source === 'amazon' && p.id === productVerified.id) {//amazon
                    p.available = true;
                }
            });
            if (p.available === undefined) {
                p.available = false;
            }
        });
        es.saveResults(products, category, (err, res) => {
            console.log('products update done successfully with err - ', err);
            if (cb) cb();
        });

    }, (err) => {
        cb();
    }).catch(()=>{
        cb();
    });
}

//Batch Updates
function bestBuyAvailableUpdate(bbyProducts, category, cb) {

    if (bbyProducts.length === 0) {
        cb('errrrrr');
        return;
    }

    const skus = bbyProducts.map((p)=>{return p.sku});
    const skus_clean = skus.filter((sku) => { return sku !== undefined});

    bestbuy.productsAvailabillityUpdate(skus_clean, (error, productsVerified) => {

        //TODO - use maping
        bbyProducts.forEach((p)=>{
            productsVerified.forEach((pv)=>{
                if (pv.sku === p.sku) {
                    p.available = pv.onlineAvailability;
                }
            })
        });
        es.saveResults(bbyProducts, category, (err, res) => {
            if (cb) cb(bbyProducts);
        });
    });
}

//MARK: - Private helpers
function scrollRecurse(scroll_id, category, shouldStop, cb) {
    if (shouldStop) {
        if (cb) cb(scroll_id)//finished
        return;
    }

    es.scrollRaw({
        scroll: '30s',
        scroll_id:scroll_id
    }, (err, res) => {
        //TODO - work, then recurse according fi we finish recurse with shuoldStop = true
        if ( !res || !res.hits) {
            if (cb) cb(scroll_id)//finished
            return;
        }
        const res_ref = res;
        const products = res.hits.hits.map(item => item._source);
        updateProducts(products, category, () => {
            scrollRecurse(res._scroll_id, category, false, cb);
        });
    })
}

module.exports = {
    run
};
