const async = require('async');
const es = require('./../engine/stores/elasticsearch');
const engine = require('./../engine/engine');
const amazonReviews = require('./providers/amazon');


function run(callback = () => {}) {
    const categories = engine.getSupportedCategories();

    async.forEachOf(categories, (category, i, callback) => {
      setTimeout( () => {
        console.log(`Running reviews scraper for category ${category}`);
        es.searchRaw({
          index: 'products',
          type: category,
          body: {
            size: 20,
            query: {
              bool: {
                must_not: {
                  exists: {
                    field: "reviewsCount",
                  }
                },
                filter: {
                  match: {
                    source: "amazon",
                  }
                }
              }
            }
          }
        },(err,res) => {
          if (err) {
            console.error(`Error searching items for category ${category}`, err);
            callback(err);
            return;
          }
          const items = res && res.hits && res.hits.hits;

          if (!items.length) {

            console.log(`No items found.`, res);
            callback();
            return;
          }

          console.log(`Found ${items.length} items to update reviews for category ${category}`);

          async.concat(items,(item,cb) => {
            item = item._source || {};
            if (item.reviews) {
              console.log(`Found reviews url for '${item.title}', fetching reviews: ${item.reviews}`);
              amazonReviews.getReviews(item.reviews,(err,res) => {
                if (err) {
                  console.error(`Error getting review for ${item.title}`, err);
                  cb();
                  return;
                }
                console.log(`Item reviews for '${item.title}: ${JSON.stringify(res)}'`);

                const updatedItem = Object.assign(res, {id: item.id});
                cb(null, updatedItem);
              });
            } else {
              console.log(`No reviews found for item ${item.title}`, item);
              cb();
            }
          },(err,res) => {
            if (err) {
              console.error('Error fetching reviews', err);
              return callback(err);
            }

            console.log(`Saving ${res.length} items with reviews to ES.`);
            es.updateResults(res, category, callback);
          })

        });
      }, i * 50000 + 1);
    }, callback);
}

module.exports = { run };